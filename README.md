# panoramax-website - EN Version

The Panoramax website was created to present and make available the tools related to the geocommun.
Take a look on [Panoramax](https://panoramax.fr/)

# Contribution

Do not hesitate if you want to contribute to the project.
To do that you can :

- Create an issue on this repository
- Create a pull request if you'd like to offer some code contribution
- Go to the [Panoramax Forum](https://forum.geocommuns.fr/c/panoramax/6) and create a post

# Tech template

This template should help get you started developing and deploy with Vue 3 in Vite.
If at some point you're lost, need help or if you have any question about the project,
you can contact us through issues or by [email](panoramax@panoramax.fr).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run workflows:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

# panoramax-website - Version FR

Le site Panoramax est là pour présenter et mettre à disposition les outils du géocommun.
Pour y accéder [c'est par ici](https://panoramax.fr/)

# Contribution

Si vous souhaitez contribuer au projet vous pouvez :

- Créer un ticket
- Créer une _pull request_ pour proposer une contribution sous forme de code
- Vous rendre sur le [Forum Panoramax](https://forum.geocommuns.fr/c/panoramax/6) et y créer un post

# Template technique

Ce Template est là pour vous aider à vous lancer dans le développement et le déploiement avec Vue 3 et Vite.
Si vous avez besoin d'aide ou une question relative au projet, n'hésitez pas à poster des issues sur gitlab
ou nous contacter [par email] (panoramax@panoramax.fr).

## Initialisation du projet

```sh
npm install
```

### Compiler et Chargement automatique pour le développement

```sh
npm run dev
```

### Type-Check, Compilation et Minification pour la production

```sh
npm run build
```

### Lancer Les tests unitaires avec [Vitest](https://vitest.dev/)

```sh
npm run workflows:unit
```

### Lint avec [ESLint](https://eslint.org/)

```sh
npm run lint
```
