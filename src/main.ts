import { createApp } from 'vue'
import VueMatomo from 'vue-matomo'
import App from './App.vue'
import router from './router'
import '@panoramax/web-viewer/build/index.css'
import './assets/main.css'
import i18n from '@/i18n'

declare global {
  interface Window {
    _paq: any[]
  }
}
const matomoHost = import.meta.env.VITE_MATOMO_HOST
const matomoSiteId = import.meta.env.VITE_MATOMO_SITE_ID
const matomoExist = matomoHost && matomoSiteId

const app = createApp(App)
app.use(i18n)
app.use(router)

if (matomoExist) {
  app.use(VueMatomo, {
    host: matomoHost,
    siteId: matomoSiteId
  })
}

app.mount('#app')

if (matomoExist) {
  window._paq.push(['trackPageView'])
}
