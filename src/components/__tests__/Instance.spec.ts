import { describe, it, expect, vi } from 'vitest'
import { shallowMount } from '@vue/test-utils'
import { createI18n } from 'vue-i18n'
import Instance from '../Instance.vue'
import fr from '../../locales/fr.json'
import * as img from '../../utils/image'

const i18n = createI18n({
  locale: 'fr',
  fallbackLocale: 'fr',
  globalInjection: true,
  legacy: false,
  messages: {
    fr
  }
})
describe('Instance', () => {
  describe('Template', () => {
    it('Should render the component with all the goods wordings', () => {
      const imgSrc = 'my-url.png'
      const imgPath = vi.spyOn(img, 'img')
      imgPath.mockReturnValue(imgSrc)
      const wrapper = shallowMount(Instance, {
        props: {
          name: 'name',
          subtitleLocalization: 'subtitleLocalization',
          descriptionLocalization: 'descriptionLocalization',
          subtitleLicense: 'subtitleLicense',
          descriptionLicense: 'descriptionLicense',
          url: 'my-url/',
          image: { alt: 'my alt', url: imgSrc }
        },
        global: {
          plugins: [i18n],
          mocks: {
            $t: (msg: string) => msg
          }
        }
      })
      expect(wrapper.html()).contains('name')
      expect(wrapper.html()).contains('subtitleLocalization')
      expect(wrapper.html()).contains('descriptionLocalization')
      expect(wrapper.html()).contains('subtitleLicense')
      expect(wrapper.html()).contains('descriptionLicense')
      expect(wrapper.html()).contains('my-url.png')
      expect(wrapper.html()).contains('text="pages.home.instance_explore"')
      expect(wrapper.html()).contains('url="my-url/"')

      expect(wrapper.html()).contains('text="pages.home.instance_contribute"')
      expect(wrapper.html()).contains('url="my-url/pourquoi-contribuer"')
    })
  })
})
