import { createI18n } from 'vue-i18n'
import en from '../locales/en.json'
import fr from '../locales/fr.json'
import { autoDetectLocale } from '../utils'

const i18n = createI18n({
  locale: autoDetectLocale(),
  fallbackLocale: 'fr',
  warnHtmlMessage: false,
  globalInjection: true,
  legacy: false,
  messages: {
    fr,
    en
  }
})
export default i18n
