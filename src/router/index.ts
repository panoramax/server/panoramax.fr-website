import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import FaqView from '../views/FaqView.vue'
import FaqItemView from '../views/FaqItemView.vue'
import UseCaseArticleItem from '../views/UseCaseArticleItem.vue'
import ContributeView from '../views/ContributeView.vue'
import CollectGuideView from '../views/CollectGuideView.vue'
import Ay11View from '../views/Ay11View.vue'
import UseCasesView from '../views/UseCasesView.vue'
import StatsView from '../views/StatsView.vue'
import LeaderboardView from '../views/LeaderboardView.vue'
import TermsAndConditionsView from '../views/TermsAndConditionsView.vue'
import PageNotFoundView from '../views/PageNotFoundView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/foire-aux-questions',
      name: 'faq',
      component: FaqView
    },
    {
      path: '/foire-aux-questions/:id',
      name: 'faqItem',
      component: FaqItemView
    },
    {
      path: '/comment-participer-a-panoramax',
      name: 'contribute',
      component: ContributeView
    },
    {
      path: '/comment-participer-a-panoramax/guide-collecte-pieton-debutant',
      name: 'collectGuide',
      component: CollectGuideView
    },
    {
      path: '/cas-d-usage',
      name: 'useCase',
      component: UseCasesView
    },
    {
      path: '/cas-d-usage/:id',
      name: 'useCaseArticleItem',
      component: UseCaseArticleItem
    },
    {
      path: '/accessibilite',
      name: 'ay11',
      component: Ay11View
    },
    {
      path: '/stats',
      name: 'stats',
      component: StatsView
    },
    {
      path: '/leaderboard',
      name: 'leaderboard',
      component: LeaderboardView
    },
    {
      path: '/mentions-legales',
      name: 'termsAndConditions',
      component: TermsAndConditionsView
    },
    { path: '/:pathMatch(.*)*', component: PageNotFoundView }
  ],
  scrollBehavior() {
    return { top: 0, left: 0 }
  }
})

export default router
