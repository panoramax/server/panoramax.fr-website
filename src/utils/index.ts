import moment from 'moment'
import 'moment/dist/locale/fr'
import fr from '@/locales/fr.json'
import en from '@/locales/en.json'
import i18n from '@/i18n'
const { tm } = i18n.global

function getSecondMondayDate(week: number) {
  function getSecondMonday(startDate: Date) {
    const startOfMonth = moment(startDate)
      .utc()
      .startOf('month')
      .startOf('isoWeek')
    let studyDate = moment(startDate)
      .utc()
      .startOf('month')
      .startOf('isoWeek')
      .add(week, 'w')

    if (studyDate.month() == startOfMonth.month()) {
      studyDate = studyDate.subtract(1, 'w')
    }
    return moment(studyDate).format('YYYY-MM-DD')
  }
  let monday
  const startOfMonth = moment().startOf('month').format('YYYY-MM-DD')
  const secondMonday = getSecondMonday(new Date(startOfMonth))
  const secondMondayIsAfterToday = moment(new Date()).isAfter(
    secondMonday,
    'days'
  )
  if (secondMondayIsAfterToday) {
    const nextMonth = moment()
      .add(1, 'months')
      .startOf('month')
      .format('YYYY-MM-DD')
    monday = getSecondMonday(new Date(nextMonth))
  } else monday = secondMonday
  return monday
}
function formatNumber(num: number): string {
  if (num < 1000) return String(num)
  if (num >= 1000000) {
    const numString = String(num)
    if (numString.length > 6) {
      const numFormatted = (num / 1000000).toFixed(1)
      const splitted = numFormatted.split('.')
      if (splitted[1] === '0') return splitted[0]
      return numFormatted
    }
  }
  return formatZeroNum(num)
}
function formatZeroNum(num: number) {
  return String(num).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')
}
function autoDetectLocale() {
  for (const navigatorLang of window.navigator.languages) {
    let language = navigatorLang
    switch (language) {
      case 'zh-TW':
      case 'zh-HK':
      case 'zh-MO':
        language = 'zh_Hant'
        break
      case 'zh-CN':
      case 'zh-SG':
        language = 'zh_Hans'
        break
      default:
        if (language.length > 2) {
          language = navigatorLang.substring(0, 2)
        }
        break
    }
    const pair = Object.entries({ fr: fr, en: en }).find(
      (pair: [string, object]) => pair[0] === language
    )
    if (pair) {
      return pair[0]
    }
  }
  return 'en'
}
interface FaqBlock {
  title: string
  text: string
}
function formatBlock(obj: any): FaqBlock[] {
  return Object.keys(obj)
    .filter((key) => key.includes('block'))
    .map((key) => obj[key])
}
function tmWrapper(key: string) {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  // this to disable the tm() error
  return tm(key) as any
}
function formatInstanceList(
  listInstance: string[]
): { name: string; color: string; logo: string }[] {
  const colorsArray = ['#0A1F69', '#f6fafe', '#2954E9', '#fdf474']
  return listInstance.map((e) => {
    const obj = { name: e }
    if (e === 'osm-fr') {
      return { ...obj, color: '#76CC6C', logo: 'osm-logo.png' }
    } else if (e === 'ign') {
      return { ...obj, color: '#71777A', logo: 'ign-logo.svg' }
    } else if (e === 'mapcomplete') {
      return { ...obj, color: '#0A1F69', logo: 'mapcomplete-logo.svg' }
    } else
      return { ...obj, color: getRandomItem(colorsArray), logo: 'logo.svg' }
  })
}
function getRandomItem(arr: string[]) {
  const randomIndex = Math.floor(Math.random() * arr.length)
  return arr[randomIndex]
}
export {
  getSecondMondayDate,
  formatNumber,
  autoDetectLocale,
  formatBlock,
  tmWrapper,
  formatInstanceList,
  formatZeroNum
}
