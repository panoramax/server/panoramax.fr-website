async function copyToClipboard(value: string): Promise<void> {
  const fullUrl = `${window.location.origin}${window.location.pathname}${value}`
  await navigator.clipboard.writeText(fullUrl).then(
    () => true,
    () => false
  )
}

export { copyToClipboard }
