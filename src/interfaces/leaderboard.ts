export interface StatsByContributor {
  id: string
  name: string
  instance_name: string
  nb_items: number
  km_captured: number
  history: {
    last_7_days: {
      nb_items: number
      km_captured: number
    }
    last_30_days: {
      nb_items: number
      km_captured: number
    }
  }
  rank?: number
}
